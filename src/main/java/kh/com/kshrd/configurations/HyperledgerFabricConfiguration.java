package kh.com.kshrd.configurations;

import kh.com.kshrd.models.fabric.*;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by PENHCHET on 7/3/18.
 */
@Configuration
public class HyperledgerFabricConfiguration {

    private static final String FABRIC_CA_COOCON = "http://178.128.217.78:7054";
    private static final String FABRIC_CA_WEBCASH_URL = "http://178.128.217.78:8054";

    public static void serialize(AppUser appUser) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(
                Paths.get(appUser.getMspId() + appUser.getName() + ".jso")))) {
            oos.writeObject(appUser);
        }
    }

    public static AppUser tryDeserialize(String name) throws Exception {
        if (Files.exists(Paths.get(name + ".jso"))) {
            return deserialize(name);
        }
        return null;
    }

    public static AppUser deserialize(String name) throws Exception {
        try (ObjectInputStream decoder = new ObjectInputStream(
                Files.newInputStream(Paths.get(name + ".jso")))) {
            return (AppUser) decoder.readObject();
        }
    }

    @Bean(name = "FABRIC_CLIENT1")
    public HFClient hfClient1() throws Exception {
        FabricOrganization organization = new FabricOrganization();
        organization.setOrganizationName("Coocon");
        organization.addFabricPeer(new FabricPeer("peer0.coocon.kshrd.com.kh", "grpc://178.128.217.78:7051", "eventhub01", "grpc://178.128.217.78:7053"));
        organization.addFabricPeer(new FabricPeer("peer1.coocon.kshrd.com.kh", "grpc://178.128.217.78:8051", "eventhub11", "grpc://178.128.217.78:8053"));
        organization.setFabricCA(new FabricCA("ca.coocon.kshrd.com.kh", FABRIC_CA_COOCON, "CooconMSP"));
        AppUser user = new AppUser();
        user.setName("admin");
        user.setEnrollmentSecret("admin");
        user.setMspId("CooconMSP");
        user.setOrganization(organization);
        return hfClient(user);
    }

    @Bean(name = "FABRIC_CLIENT2")
    public HFClient hfClient2() throws Exception {
        FabricOrganization organization = new FabricOrganization();
        organization.setOrganizationName("Webcash");
        organization.addFabricPeer(new FabricPeer("peer0.webcash.kshrd.com.kh", "grpc://178.128.217.78:9051", "eventhub02", "grpc://178.128.217.78:9053"));
        organization.addFabricPeer(new FabricPeer("peer1.webcash.kshrd.com.kh", "grpc://178.128.217.78:10051", "eventhub12", "grpc://178.128.217.78:10053"));
        organization.setFabricCA(new FabricCA("ca.webcash.kshrd.com.kh", FABRIC_CA_WEBCASH_URL, "WebcashMSP"));
        AppUser user = new AppUser();
        user.setName("admin");
        user.setEnrollmentSecret("admin");
        user.setMspId("WebcashMSP");
        user.setOrganization(organization);
        return hfClient(user);
    }

    @Bean(name = "HF_CLIENT")
    public HFClient hfClient() throws Exception {
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        // setup the client
        HFClient client = HFClient.createNewInstance();
        client.setCryptoSuite(cryptoSuite);
        return client;
    }

    private HFClient hfClient(AppUser appUser) throws Exception {
        HFCAClient caClient = HyperledgerFabricSDK.getHfCaClient(appUser.getOrganization().getFabricCA().getUrl(), null);
        AppUser admin = tryDeserialize(appUser.getMspId() + appUser.getName());
        if (admin == null) {

            Enrollment adminEnrollment = caClient.enroll(appUser.getName(), appUser.getEnrollmentSecret());
            admin = new AppUser(appUser.getName(), appUser.getOrganization().getOrganizationName(), appUser.getOrganization().getFabricCA().getMspId(), adminEnrollment);
            admin.getRoles().add("ADMIN" + appUser.getOrganization().getOrganizationName());
            admin.setMspId(appUser.getOrganization().getFabricCA().getMspId());
            admin.setEnrollmentSecret(appUser.getEnrollmentSecret());

            FabricOrganization organization = new FabricOrganization();
            organization.setOrganizationName(appUser.getOrganization().getOrganizationName());
            organization.setFabricPeers(appUser.getOrganization().getFabricPeers());
            organization.setFabricCA(appUser.getOrganization().getFabricCA());
            admin.setName(appUser.getName());
            admin.setEnrollmentSecret(appUser.getEnrollmentSecret());
            admin.setOrganization(organization);
            serialize(admin);
        }
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        // setup the client
        HFClient client = HFClient.createNewInstance();
        client.setCryptoSuite(cryptoSuite);
        client.setUserContext(admin);
        return client;
    }
}
