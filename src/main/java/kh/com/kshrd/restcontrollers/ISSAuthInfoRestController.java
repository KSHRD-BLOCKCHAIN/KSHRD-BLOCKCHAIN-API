package kh.com.kshrd.restcontrollers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.ByteString;
import kh.com.kshrd.models.ISSAuthInfo;
import kh.com.kshrd.models.ISSAuthInfoRequest;
import kh.com.kshrd.models.ISSAuthInfoResponse;
import kh.com.kshrd.models.fabric.AppUser;
import kh.com.kshrd.models.fabric.HyperledgerFabricSDK;
import kh.com.kshrd.utils.Pagination;
import kh.com.kshrd.utils.ResponseList;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
public class ISSAuthInfoRestController {

    private static final Logger log = Logger.getLogger(ISSAuthInfoRestController.class);

    @Autowired
    @Qualifier("HF_CLIENT")
    private HFClient hfClient;

    @RequestMapping(value = "/v1/api/bc/issauthinfos", method = RequestMethod.GET)
    public ResponseList<ISSAuthInfo> queryAllISSAuthInfo() throws Exception {

        ResponseList<ISSAuthInfo> responseList = new ResponseList<>();

        AppUser user = HyperledgerFabricSDK.tryDeserialize("CooconMSPadmin");

        hfClient.setUserContext(user);

        Channel channel = HyperledgerFabricSDK.getChannel(hfClient, user);

        // create chaincode request
        QueryByChaincodeRequest qpr = hfClient.newQueryProposalRequest();

        // build cc id providing the chaincode name. Version is omitted here.
        ChaincodeID kshrdSmartContractCCId = ChaincodeID.newBuilder()
                .setName("kshrdsmartcontract")
                .setVersion("1.0")
                .build();
        qpr.setChaincodeID(kshrdSmartContractCCId);
        // CC function to be called
        qpr.setFcn("queryAllIssAuthInfo");

        Collection<ProposalResponse> responses = channel.queryByChaincode(qpr);
        Gson gson = new Gson();
        for (ProposalResponse response : responses) {
            if (response.isVerified() && response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                ByteString payload = response.getProposalResponse().getResponse().getPayload();
                System.out.println(payload.toStringUtf8());

                List<ISSAuthInfo> issAuthInfos = gson.fromJson(payload.toStringUtf8(), new TypeToken<List<ISSAuthInfo>>() {
                }.getType());


                responseList.setData(issAuthInfos);
                Pagination pagination = new Pagination();
                pagination.setTotalCount((long) issAuthInfos.size());
                responseList.setPagination(pagination);
                responseList.setCode("0000");

                System.out.println(issAuthInfos);
            } else {
                log.error("response failed. status: " + response.getStatus().getStatus());
            }
        }

        return responseList;
    }

    @RequestMapping(value = "/v1/api/bc/issauthinfos/{txId}", method = RequestMethod.GET)
    public Map<String, Object> queryAllISSAuthInfoByTxId(@PathVariable("txId") String txId) throws Exception {

        ResponseList<ISSAuthInfo> responseList = new ResponseList<>();

        AppUser user = HyperledgerFabricSDK.tryDeserialize("CooconMSPadmin");

        hfClient.setUserContext(user);

        Channel channel = HyperledgerFabricSDK.getChannel(hfClient, user);

        BlockInfo blockInfo = channel.queryBlockByTransactionID(txId);

        System.out.println("BLOCK INFO ===> " + blockInfo.getBlock().getData().getData(0).toStringUtf8());

        TransactionInfo info = channel.queryTransactionByID(txId);

        ByteString payload = info.getProcessedTransaction().getTransactionEnvelope().getPayload();

//        payload.writeTo(new FileOutputStream(new File("Tx.txt")));

//        Gson gson = new Gson();
//        String json = gson.fromJson(payload.toStringUtf8(), String.class);

        System.out.println(payload.toStringUtf8());


        Map<String, Object> response = new HashMap<>();
        response.put("TxId", txId);

        return response;
    }

    @RequestMapping(value = "/v1/api/bc/issauthinfos", method = RequestMethod.POST)
    public ISSAuthInfoResponse issAuthInfoRequest(@RequestBody ISSAuthInfoRequest issAuthInfoRequest) throws Exception {

        ISSAuthInfoResponse issAuthInfoResponse = new ISSAuthInfoResponse();

        AppUser user = HyperledgerFabricSDK.tryDeserialize("CooconMSPadmin");

        hfClient.setUserContext(user);

        Channel channel = HyperledgerFabricSDK.getChannel(hfClient, user);

        switch (issAuthInfoRequest.getRequestMode()) {
            case "R":
                // create chaincode request
                QueryByChaincodeRequest queryByChaincodeRequest = hfClient.newQueryProposalRequest();

                // build cc id providing the chaincode name. Version is omitted here.
                ChaincodeID kshrdSmartContractCCId = ChaincodeID.newBuilder()
                        .setName("kshrdsmartcontract")
                        .setVersion("1.0")
                        .build();
                queryByChaincodeRequest.setChaincodeID(kshrdSmartContractCCId);
                // CC function to be called
                queryByChaincodeRequest.setFcn("queryIssAuthInfo");
                queryByChaincodeRequest.setArgs(new String[]{
                        issAuthInfoRequest.getUuid()
                });

                Collection<ProposalResponse> queryProposals = channel.queryByChaincode(queryByChaincodeRequest, channel.getPeers());
                for (ProposalResponse proposalResponse : queryProposals) {
                    if (!proposalResponse.isVerified() || proposalResponse.getStatus() != ProposalResponse.Status.SUCCESS) {
                        System.err.println("Failed query proposal from peer " + proposalResponse.getPeer().getName() + " status: " + proposalResponse.getStatus() +
                                ". Messages: " + proposalResponse.getMessage()
                                + ". Was verified : " + proposalResponse.isVerified());

                    } else {
                        String payload = proposalResponse.getProposalResponse().getResponse().getPayload().toStringUtf8();
                        System.out.printf("Query payload of b from peer %s returned %s", proposalResponse.getPeer().getName(), payload);
                    }
                }


                Gson gson = new Gson();

                for (ProposalResponse proposalResponse : queryProposals) {
                    if (proposalResponse.isVerified() && proposalResponse.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                        ByteString payload = proposalResponse.getProposalResponse().getResponse().getPayload();
                        System.out.println(payload.toStringUtf8());
                        ISSAuthInfo issAuthInfo = gson.fromJson(payload.toStringUtf8(), new TypeToken<ISSAuthInfo>() {
                        }.getType());
                        issAuthInfoResponse.setResultCode("0000");
                        issAuthInfoResponse.setResultMessage("Successfully");
                        issAuthInfoResponse.setUuid(issAuthInfo.getUuid());
                        issAuthInfoResponse.setCompanyIdNo(issAuthInfo.getCompanyIdNo());
                        issAuthInfoResponse.setApiKey(issAuthInfo.getApiKey());
                        issAuthInfoResponse.setBizCd(issAuthInfo.getBizCd());
                        issAuthInfoResponse.setWebID(issAuthInfo.getWebId());
                        issAuthInfoResponse.setWebPwd(issAuthInfo.getWebPwd());
                        System.out.println(issAuthInfo);
                    } else {
                        System.err.println("Failed query proposal from peer " + proposalResponse.getPeer().getName() + " status: " + proposalResponse.getStatus() +
                                ". Messages: " + proposalResponse.getMessage()
                                + ". Was verified : " + proposalResponse.isVerified());
                    }
                }
                break;
            case "C":

                TransactionProposalRequest transactionProposalRequest = hfClient.newTransactionProposalRequest();
                ChaincodeID cid = ChaincodeID.newBuilder().setName("kshrdsmartcontract").build();
                transactionProposalRequest.setChaincodeID(cid);
                transactionProposalRequest.setFcn("addIssAuthInfo");
                String uuid = UUID.randomUUID().toString();
                transactionProposalRequest.setArgs(new String[]{
                        UUID.randomUUID().toString(),
                        issAuthInfoRequest.getCompanyIdNo(),
                        issAuthInfoRequest.getApiKey(),
                        issAuthInfoRequest.getBizCd(),
                        issAuthInfoRequest.getWebID(),
                        issAuthInfoRequest.getWebPwd()
                });

                BlockEvent.TransactionEvent event = sendTransaction(hfClient, channel, transactionProposalRequest).get(100, TimeUnit.SECONDS);

                if (event.isValid()) {
                    log.info("Transacion tx: " + event.getTransactionID() + " is completed.");
                    issAuthInfoResponse.setResultCode("0000");
                    issAuthInfoResponse.setResultMessage("New ISS_AUTHINFO has been created.");
                    issAuthInfoResponse.setUuid(uuid);
                    issAuthInfoResponse.setCompanyIdNo(issAuthInfoRequest.getCompanyIdNo());
                    issAuthInfoResponse.setApiKey(issAuthInfoRequest.getApiKey());
                    issAuthInfoResponse.setBizCd(issAuthInfoRequest.getBizCd());
                    issAuthInfoResponse.setWebID(issAuthInfoRequest.getWebID());
                    issAuthInfoResponse.setWebPwd(issAuthInfoRequest.getWebPwd());
                } else {
                    log.error("Transaction tx: " + event.getTransactionID() + " is invalid.");
                    issAuthInfoResponse.setResultCode("9999");
                    issAuthInfoResponse.setResultMessage("There is an error when creating new ISSAuthInfo.");
                }
                break;
            default:
                issAuthInfoResponse.setResultCode("9999");
                issAuthInfoResponse.setResultMessage("REQ_MODE value is only 'R' or 'C'");
                break;
        }
        return issAuthInfoResponse;
    }

    private static CompletableFuture<BlockEvent.TransactionEvent> sendTransaction(HFClient client, Channel channel, TransactionProposalRequest transactionProposalRequest)
            throws InvalidArgumentException, ProposalException {
        Collection<ProposalResponse> responses = channel.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
        List<ProposalResponse> invalid = responses.stream().filter(r -> r.isInvalid()).collect(Collectors.toList());
        if (!invalid.isEmpty()) {
            invalid.forEach(response -> {
                log.error(response.getMessage());
            });
            throw new RuntimeException("invalid response(s) found");
        }
        return channel.sendTransaction(responses);
    }
}