package kh.com.kshrd.restcontrollers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.ByteString;
import kh.com.kshrd.models.ISSCertInfo;
import kh.com.kshrd.models.ISSCertInfoRequest;
import kh.com.kshrd.models.fabric.AppUser;
import kh.com.kshrd.models.fabric.HyperledgerFabricSDK;
import kh.com.kshrd.utils.Pagination;
import kh.com.kshrd.utils.ResponseList;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
public class ISSCertInfoRestController {

    private static final Logger log = Logger.getLogger(ISSCertInfoRestController.class);

    @Autowired
    @Qualifier("HF_CLIENT")
    private HFClient hfClient;

    @RequestMapping(value = "/v1/api/bc/isscertinfos", method = RequestMethod.GET)
    public ResponseList<ISSCertInfo> queryAllISSCertInfo() throws Exception {

        ResponseList<ISSCertInfo> responseList = new ResponseList<>();

        AppUser user = HyperledgerFabricSDK.tryDeserialize("CooconMSPadmin");

        hfClient.setUserContext(user);

        Channel channel = HyperledgerFabricSDK.getChannel(hfClient, user);

        // create chaincode request
        QueryByChaincodeRequest qpr = hfClient.newQueryProposalRequest();

        // build cc id providing the chaincode name. Version is omitted here.
        ChaincodeID kshrdSmartContractCCId = ChaincodeID.newBuilder()
                .setName("kshrdsmartcontract")
                .setVersion("1.0")
                .build();
        qpr.setChaincodeID(kshrdSmartContractCCId);
        // CC function to be called
        qpr.setFcn("queryAllIssCertInfo");

        Collection<ProposalResponse> responses = channel.queryByChaincode(qpr);
        Gson gson = new Gson();
        for (ProposalResponse response : responses) {
            if (response.isVerified() && response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                ByteString payload = response.getProposalResponse().getResponse().getPayload();
                System.out.println(payload.toStringUtf8());

                List<ISSCertInfo> issCertInfos = gson.fromJson(payload.toStringUtf8(), new TypeToken<List<ISSCertInfo>>() {
                }.getType());


                responseList.setData(issCertInfos);
                Pagination pagination = new Pagination();
                pagination.setTotalCount((long) issCertInfos.size());
                responseList.setPagination(pagination);
                responseList.setCode("0000");

                System.out.println(issCertInfos);
            } else {
                log.error("response failed. status: " + response.getStatus().getStatus());
            }
        }

        return responseList;
    }

    @RequestMapping(value = "/v1/api/bc/isscertinfos", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> addISSCertInfo(@RequestBody ISSCertInfoRequest.ISSCertInfoWriteRequest issCertInfoWriteRequest) throws Exception {
        Map<String, Object> response = new HashMap<>();

        AppUser user = HyperledgerFabricSDK.tryDeserialize("CooconMSPadmin");

        hfClient.setUserContext(user);

        Channel channel = HyperledgerFabricSDK.getChannel(hfClient, user);

        switch (issCertInfoWriteRequest.getRequestMode()) {
            case "R":
                // create chaincode request
                QueryByChaincodeRequest queryByChaincodeRequest = hfClient.newQueryProposalRequest();

                // build cc id providing the chaincode name. Version is omitted here.
                ChaincodeID kshrdSmartContractCCId = ChaincodeID.newBuilder()
                        .setName("kshrdsmartcontract")
                        .setVersion("1.0")
                        .build();
                queryByChaincodeRequest.setChaincodeID(kshrdSmartContractCCId);
                // CC function to be called
                queryByChaincodeRequest.setFcn("queryIssCertInfo");
                queryByChaincodeRequest.setArgs(new String[]{
                        issCertInfoWriteRequest.getApiKey()
                });

                Collection<ProposalResponse> queryProposals = channel.queryByChaincode(queryByChaincodeRequest, channel.getPeers());

                Gson gson = new Gson();

                for (ProposalResponse proposalResponse : queryProposals) {
                    if (proposalResponse.isVerified() && proposalResponse.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                        ByteString payload = proposalResponse.getProposalResponse().getResponse().getPayload();
                        System.out.println(payload.toStringUtf8());

                        ISSCertInfo issCertInfo = gson.fromJson(payload.toStringUtf8(), new TypeToken<ISSCertInfo>() {
                        }.getType());

                        response.put("RESULT_CD", "0000");
                        response.put("API_KEY", issCertInfo.getAPIKey());
                        response.put("CERTORG", issCertInfo.getCertOrg());
                        response.put("CERTNAME", issCertInfo.getCertName());
                        response.put("CERTDATE", issCertInfo.getCertDate());
                        response.put("CERTPWD", issCertInfo.getCertPWD());
                        response.put("CERTPATH", issCertInfo.getCertPath());
                        response.put("CERTDATA", issCertInfo.getCertData());
                        System.out.println(issCertInfo);

                    } else {
                        System.err.println("Failed query proposal from peer " + proposalResponse.getPeer().getName() + " status: " + proposalResponse.getStatus() +
                                ". Messages: " + proposalResponse.getMessage()
                                + ". Was verified : " + proposalResponse.isVerified());

                        response.put("RESULT_CD", "9999");
                        response.put("RESULT_MESSAGE", "Failed query proposal from peer " + proposalResponse.getPeer().getName() + " status: " + proposalResponse.getStatus() +
                                ". Messages: " + proposalResponse.getMessage()
                                + ". Was verified : " + proposalResponse.isVerified());
                        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
                break;
            case "C":

                TransactionProposalRequest transactionProposalRequest = hfClient.newTransactionProposalRequest();
                ChaincodeID cid = ChaincodeID.newBuilder().setName("kshrdsmartcontract").build();
                transactionProposalRequest.setChaincodeID(cid);
                transactionProposalRequest.setFcn("addIssCertInfo");
                String uuid = UUID.randomUUID().toString();
                transactionProposalRequest.setArgs(new String[]{
                        issCertInfoWriteRequest.getCompanyIdNo(),
                        issCertInfoWriteRequest.getApiKey(),
                        issCertInfoWriteRequest.getBizCd(),
                        issCertInfoWriteRequest.getCertOrg(),
                        issCertInfoWriteRequest.getCertName(),
                        issCertInfoWriteRequest.getCertDate(),
                        issCertInfoWriteRequest.getCertPwd(),
                        issCertInfoWriteRequest.getCertPath(),
                        issCertInfoWriteRequest.getCertData(),
                });

                BlockEvent.TransactionEvent event = sendTransaction(hfClient, channel, transactionProposalRequest).get(60, TimeUnit.SECONDS);

                if (event.isValid()) {
                    log.info("Transacion tx: " + event.getTransactionID() + " is completed.");

                    response.put("RESULT_CD","0000");
                    response.put("RESULT_MESSAGE", "Successfully");
                } else {
                    log.error("Transaction tx: " + event.getTransactionID() + " is invalid.");
                    response.put("RESULT_CD", "9999");
                    response.put("RESULT_MESSAGE", "There is an error when creating new ISSAuthInfo.");
                    return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
                }
                break;
            default:
                response.put("RESULT_CD", "9999");
                response.put("RESULT_MESSAGE", "REQ_MODE value is only 'R' or 'C'");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private static CompletableFuture<BlockEvent.TransactionEvent> sendTransaction(HFClient client, Channel channel, TransactionProposalRequest transactionProposalRequest)
            throws InvalidArgumentException, ProposalException {
        Collection<ProposalResponse> responses = channel.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
        List<ProposalResponse> invalid = responses.stream().filter(r -> r.isInvalid()).collect(Collectors.toList());
        if (!invalid.isEmpty()) {
            invalid.forEach(response -> {
                log.error(response.getMessage());
            });
            throw new RuntimeException("invalid response(s) found");
        }
        return channel.sendTransaction(responses);
    }

}
