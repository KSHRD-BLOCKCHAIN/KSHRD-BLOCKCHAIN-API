package kh.com.kshrd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KshrdBlockchainApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(KshrdBlockchainApiApplication.class, args);
    }
}
