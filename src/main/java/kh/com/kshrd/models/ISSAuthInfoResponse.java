package kh.com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ISSAuthInfoResponse {

    @JsonProperty("RESULT_CD")
    private String resultCode;
    @JsonProperty("RESULT_MSG")
    private String resultMessage;
    @JsonProperty("UUID")
    private String uuid;
    @JsonProperty("COMP_IDNO")
    private String companyIdNo;
    @JsonProperty("API_KEY")
    private String apiKey;
    @JsonProperty("BIZ_CD")
    private String bizCd;
    @JsonProperty("WEB_ID")
    private String webID;
    @JsonProperty("WEB_PW")
    private String webPwd;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCompanyIdNo() {
        return companyIdNo;
    }

    public void setCompanyIdNo(String companyIdNo) {
        this.companyIdNo = companyIdNo;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getBizCd() {
        return bizCd;
    }

    public void setBizCd(String bizCd) {
        this.bizCd = bizCd;
    }

    public String getWebID() {
        return webID;
    }

    public void setWebID(String webID) {
        this.webID = webID;
    }

    public String getWebPwd() {
        return webPwd;
    }

    public void setWebPwd(String webPwd) {
        this.webPwd = webPwd;
    }

    @Override
    public String toString() {
        return "ISSAuthInfoResponse{" +
                "resultCode='" + resultCode + '\'' +
                ", resultMessage='" + resultMessage + '\'' +
                ", uuid='" + uuid + '\'' +
                ", companyIdNo='" + companyIdNo + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", bizCd='" + bizCd + '\'' +
                ", webID='" + webID + '\'' +
                ", webPwd='" + webPwd + '\'' +
                '}';
    }
}
