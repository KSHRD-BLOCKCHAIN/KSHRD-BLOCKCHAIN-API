package kh.com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ISSCertInfoRequest {

    public static class ISSCertInfoWriteRequest {

        @JsonProperty("REQ_MODE")
        private String requestMode;
        @JsonProperty("COMP_IDNO")
        private String companyIdNo;
        @JsonProperty("API_KEY")
        private String apiKey;
        @JsonProperty("BIZ_CD")
        private String bizCd;
        @JsonProperty("CERT_ORG")
        private String certOrg;
        @JsonProperty("CERT_NAME")
        private String certName;
        @JsonProperty("CERT_DATE")
        private String certDate;
        @JsonProperty("CERT_PWD")
        private String certPwd;
        @JsonProperty("CERT_PATH")
        private String certPath;
        @JsonProperty("CERT_DATA")
        private String certData;

        public String getRequestMode() {
            return requestMode;
        }

        public void setRequestMode(String requestMode) {
            this.requestMode = requestMode;
        }

        public String getCompanyIdNo() {
            return companyIdNo;
        }

        public void setCompanyIdNo(String companyIdNo) {
            this.companyIdNo = companyIdNo;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public String getBizCd() {
            return bizCd;
        }

        public void setBizCd(String bizCd) {
            this.bizCd = bizCd;
        }

        public String getCertOrg() {
            return certOrg;
        }

        public void setCertOrg(String certOrg) {
            this.certOrg = certOrg;
        }

        public String getCertName() {
            return certName;
        }

        public void setCertName(String certName) {
            this.certName = certName;
        }

        public String getCertDate() {
            return certDate;
        }

        public void setCertDate(String certDate) {
            this.certDate = certDate;
        }

        public String getCertPwd() {
            return certPwd;
        }

        public void setCertPwd(String certPwd) {
            this.certPwd = certPwd;
        }

        public String getCertPath() {
            return certPath;
        }

        public void setCertPath(String certPath) {
            this.certPath = certPath;
        }

        public String getCertData() {
            return certData;
        }

        public void setCertData(String certData) {
            this.certData = certData;
        }

        @Override
        public String toString() {
            return "ISSCertInfoWriteRequest{" +
                    "requestMode='" + requestMode + '\'' +
                    ", companyIdNo='" + companyIdNo + '\'' +
                    ", apiKey='" + apiKey + '\'' +
                    ", bizCd='" + bizCd + '\'' +
                    ", certOrg='" + certOrg + '\'' +
                    ", certName='" + certName + '\'' +
                    ", certDate='" + certDate + '\'' +
                    ", certPwd='" + certPwd + '\'' +
                    ", certPath='" + certPath + '\'' +
                    ", certData='" + certData + '\'' +
                    '}';
        }
    }


    public static class ISSCertInfoReadRequest {

        @JsonProperty("REQ_MODE")
        private String requestMode;
        @JsonProperty("API_KEY")
        private String apiKey;

        public String getRequestMode() {
            return requestMode;
        }

        public void setRequestMode(String requestMode) {
            this.requestMode = requestMode;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        @Override
        public String toString() {
            return "ISSCertInfoReadRequest{" +
                    "reqMode='" + requestMode + '\'' +
                    ", apiKey='" + apiKey + '\'' +
                    '}';
        }
    }

}
