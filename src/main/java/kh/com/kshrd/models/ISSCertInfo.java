package kh.com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class ISSCertInfo {

    @SerializedName("COMP_IDNO")
    @JsonProperty("COMP_IDNO")
    private String CompanyIDNO;
    @SerializedName("API_KEY")
    @JsonProperty("API_KEY")
    private String APIKey;
    @SerializedName("BIZ_CD")
    @JsonProperty("BIZ_CD")
    private String BizCD;
    @SerializedName("CERTORG")
    @JsonProperty("CERTORG")
    private String CertOrg;
    @SerializedName("CERTNAME")
    @JsonProperty("CERTNAME")
    private String CertName;
    @SerializedName("CERTDATE")
    @JsonProperty("CERTDATE")
    private String CertDate;
    @SerializedName("CERTPWD")
    @JsonProperty("CERTPWD")
    private String CertPWD;
    @SerializedName("CERTPATH")
    @JsonProperty("CERTPATH")
    private String CertPath;
    @SerializedName("CERTDATA")
    @JsonProperty("CERTDATA")
    private String CertData;
    @SerializedName("CERTDATA1")
    @JsonProperty("CERTDATA1")
    private String CertData1;
    @SerializedName("CERTDATA2")
    @JsonProperty("CERTDATA2")
    private String CertData2;
    @SerializedName("CERTDATA3")
    @JsonProperty("CERTDATA3")
    private String CertData3;
    @SerializedName("CERTDATA4")
    @JsonProperty("CERTDATA4")
    private String CertData4;
    @SerializedName("CERTDATA5")
    @JsonProperty("CERTDATA5")
    private String CertData5;
    @SerializedName("CERTDATA6")
    @JsonProperty("CERTDATA6")
    private String CertData6;
    @SerializedName("CERTDATA7")
    @JsonProperty("CERTDATA7")
    private String CertData7;
    @SerializedName("CERTDATA8")
    @JsonProperty("CERTDATA8")
    private String CertData8;
    @SerializedName("CERTDATA9")
    @JsonProperty("CERTDATA9")
    private String CertData9;

    public String getCompanyIDNO() {
        return CompanyIDNO;
    }

    public void setCompanyIDNO(String companyIDNO) {
        CompanyIDNO = companyIDNO;
    }

    public String getAPIKey() {
        return APIKey;
    }

    public void setAPIKey(String APIKey) {
        this.APIKey = APIKey;
    }

    public String getBizCD() {
        return BizCD;
    }

    public void setBizCD(String bizCD) {
        BizCD = bizCD;
    }

    public String getCertOrg() {
        return CertOrg;
    }

    public void setCertOrg(String certOrg) {
        CertOrg = certOrg;
    }

    public String getCertName() {
        return CertName;
    }

    public void setCertName(String certName) {
        CertName = certName;
    }

    public String getCertDate() {
        return CertDate;
    }

    public void setCertDate(String certDate) {
        CertDate = certDate;
    }

    public String getCertPWD() {
        return CertPWD;
    }

    public void setCertPWD(String certPWD) {
        CertPWD = certPWD;
    }

    public String getCertPath() {
        return CertPath;
    }

    public void setCertPath(String certPath) {
        CertPath = certPath;
    }

    public String getCertData() {
        return CertData;
    }

    public void setCertData(String certData) {
        CertData = certData;
    }

    public String getCertData1() {
        return CertData1;
    }

    public void setCertData1(String certData1) {
        CertData1 = certData1;
    }

    public String getCertData2() {
        return CertData2;
    }

    public void setCertData2(String certData2) {
        CertData2 = certData2;
    }

    public String getCertData3() {
        return CertData3;
    }

    public void setCertData3(String certData3) {
        CertData3 = certData3;
    }

    public String getCertData4() {
        return CertData4;
    }

    public void setCertData4(String certData4) {
        CertData4 = certData4;
    }

    public String getCertData5() {
        return CertData5;
    }

    public void setCertData5(String certData5) {
        CertData5 = certData5;
    }

    public String getCertData6() {
        return CertData6;
    }

    public void setCertData6(String certData6) {
        CertData6 = certData6;
    }

    public String getCertData7() {
        return CertData7;
    }

    public void setCertData7(String certData7) {
        CertData7 = certData7;
    }

    public String getCertData8() {
        return CertData8;
    }

    public void setCertData8(String certData8) {
        CertData8 = certData8;
    }

    public String getCertData9() {
        return CertData9;
    }

    public void setCertData9(String certData9) {
        CertData9 = certData9;
    }

    @Override
    public String toString() {
        return "ISSCertInfo{" +
                "CompanyIDNO='" + CompanyIDNO + '\'' +
                ", APIKey='" + APIKey + '\'' +
                ", BizCD='" + BizCD + '\'' +
                ", CertOrg='" + CertOrg + '\'' +
                ", CertName='" + CertName + '\'' +
                ", CertDate='" + CertDate + '\'' +
                ", CertPWD='" + CertPWD + '\'' +
                ", CertPath='" + CertPath + '\'' +
                ", CertData='" + CertData + '\'' +
                ", CertData1='" + CertData1 + '\'' +
                ", CertData2='" + CertData2 + '\'' +
                ", CertData3='" + CertData3 + '\'' +
                ", CertData4='" + CertData4 + '\'' +
                ", CertData5='" + CertData5 + '\'' +
                ", CertData6='" + CertData6 + '\'' +
                ", CertData7='" + CertData7 + '\'' +
                ", CertData8='" + CertData8 + '\'' +
                ", CertData9='" + CertData9 + '\'' +
                '}';
    }
}
