package kh.com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public class ISSAuthInfo {
    @SerializedName("UUID")
    @JsonProperty("UUID")
    private String uuid;
    @JsonProperty("COMP_IDNO")
    @SerializedName("COMP_IDNO")
    private String companyIdNo;
    @JsonProperty("API_KEY")
    @SerializedName("API_KEY")
    private String apiKey;
    @JsonProperty("BIZ_CD")
    @SerializedName("BIZ_CD")
    private String bizCd;
    @JsonProperty("WEB_ID")
    @SerializedName("WEB_ID")
    private String webId;
    @JsonProperty("WEB_PW")
    @SerializedName("WEB_PW")
    private String webPwd;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCompanyIdNo() {
        return companyIdNo;
    }

    public void setCompanyIdNo(String companyIdNo) {
        this.companyIdNo = companyIdNo;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getBizCd() {
        return bizCd;
    }

    public void setBizCd(String bizCd) {
        this.bizCd = bizCd;
    }

    public String getWebId() {
        return webId;
    }

    public void setWebId(String webId) {
        this.webId = webId;
    }

    public String getWebPwd() {
        return webPwd;
    }

    public void setWebPwd(String webPwd) {
        this.webPwd = webPwd;
    }

    @Override
    public String toString() {
        return "ISSAuthInfo{" +
                "uuid='" + uuid + '\'' +
                ", companyIdNo='" + companyIdNo + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", bizCd='" + bizCd + '\'' +
                ", webId='" + webId + '\'' +
                ", webPwd='" + webPwd + '\'' +
                '}';
    }
}