package kh.com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ISSCertInfoResponse {

    public static class ISSCertInfoWriteResponse {
        @JsonProperty("RESULT_CD")
        private String resultCd;
        @JsonProperty("API_KEY")
        private String apiKey;

        public String getResultCd() {
            return resultCd;
        }

        public void setResultCd(String resultCd) {
            this.resultCd = resultCd;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        @Override
        public String toString() {
            return "ISSCertInfoWriteResponse{" +
                    "resultCd='" + resultCd + '\'' +
                    ", apiKey='" + apiKey + '\'' +
                    '}';
        }
    }

    public static class ISSCertInfoReadResponse {
        @JsonProperty("RESULT_CD")
        private String resultCd;
        @JsonProperty("API_KEY")
        private String apiKey;
        @JsonProperty("CERT_ORG")
        private String certOrg;
        @JsonProperty("CERT_NAME")
        private String certName;
        @JsonProperty("CERT_DATE")
        private String certDate;
        @JsonProperty("CERT_PWD")
        private String certPwd;
        @JsonProperty("CERT_PATH")
        private String certPath;
        @JsonProperty("CERT_DATA")
        private String certData;

        public String getResultCd() {
            return resultCd;
        }

        public void setResultCd(String resultCd) {
            this.resultCd = resultCd;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public String getCertOrg() {
            return certOrg;
        }

        public void setCertOrg(String certOrg) {
            this.certOrg = certOrg;
        }

        public String getCertName() {
            return certName;
        }

        public void setCertName(String certName) {
            this.certName = certName;
        }

        public String getCertDate() {
            return certDate;
        }

        public void setCertDate(String certDate) {
            this.certDate = certDate;
        }

        public String getCertPwd() {
            return certPwd;
        }

        public void setCertPwd(String certPwd) {
            this.certPwd = certPwd;
        }

        public String getCertPath() {
            return certPath;
        }

        public void setCertPath(String certPath) {
            this.certPath = certPath;
        }

        public String getCertData() {
            return certData;
        }

        public void setCertData(String certData) {
            this.certData = certData;
        }

        @Override
        public String toString() {
            return "ISSCertInfoReadResponse{" +
                    "resultCd='" + resultCd + '\'' +
                    ", apiKey='" + apiKey + '\'' +
                    ", certOrg='" + certOrg + '\'' +
                    ", certName='" + certName + '\'' +
                    ", certDate='" + certDate + '\'' +
                    ", certPwd='" + certPwd + '\'' +
                    ", certPath='" + certPath + '\'' +
                    ", certData='" + certData + '\'' +
                    '}';
        }
    }
}
