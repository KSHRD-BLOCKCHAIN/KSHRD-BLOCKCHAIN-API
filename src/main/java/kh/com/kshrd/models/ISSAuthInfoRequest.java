package kh.com.kshrd.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"REQ_MODE", "COMP_IDNO", "API_KEY", "BIZ_CD", "WEB_ID", "WEB_PW"})
public class ISSAuthInfoRequest {

    @JsonProperty("UUID")
    private String uuid;
    @JsonProperty("REQ_MODE")
    private String requestMode;
    @JsonProperty("COMP_IDNO")
    private String companyIdNo;
    @JsonProperty("API_KEY")
    private String apiKey;
    @JsonProperty("BIZ_CD")
    private String bizCd;
    @JsonProperty("WEB_ID")
    private String webID;
    @JsonProperty("WEB_PW")
    private String webPwd;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getRequestMode() {
        return requestMode;
    }

    public void setRequestMode(String requestMode) {
        this.requestMode = requestMode;
    }

    public String getCompanyIdNo() {
        return companyIdNo;
    }

    public void setCompanyIdNo(String companyIdNo) {
        this.companyIdNo = companyIdNo;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getBizCd() {
        return bizCd;
    }

    public void setBizCd(String bizCd) {
        this.bizCd = bizCd;
    }

    public String getWebID() {
        return webID;
    }

    public void setWebID(String webID) {
        this.webID = webID;
    }

    public String getWebPwd() {
        return webPwd;
    }

    public void setWebPwd(String webPwd) {
        this.webPwd = webPwd;
    }

    @Override
    public String toString() {
        return "ISSAuthInfoRequest{" +
                "uuid='" + uuid + '\'' +
                ", requestMode='" + requestMode + '\'' +
                ", companyIdNo='" + companyIdNo + '\'' +
                ", apiKey='" + apiKey + '\'' +
                ", bizCd='" + bizCd + '\'' +
                ", webID='" + webID + '\'' +
                ", webPwd='" + webPwd + '\'' +
                '}';
    }
}
