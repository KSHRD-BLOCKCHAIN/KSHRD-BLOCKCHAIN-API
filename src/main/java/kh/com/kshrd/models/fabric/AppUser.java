package kh.com.kshrd.models.fabric;

import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.User;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * <h1>AppUser</h1>
 * <p>
 * Basic implementation of the {@link User} interface.
 */
public class AppUser implements User, Serializable {
    private static final long serializationId = 1L;

    private String name;
    private Set<String> roles = new HashSet<>();
    private String account;
    private String affiliation;
    private String enrollmentSecret;
    private Enrollment enrollment;
    private String mspId;
    private FabricOrganization organization;

    public AppUser() {
        // no-arg constructor
    }

    public AppUser(String name, String affiliation, String mspId, Enrollment enrollment) {
        this.name = name;
        this.affiliation = affiliation;
        this.enrollment = enrollment;
        this.mspId = mspId;
    }

    public static long getSerializationId() {
        return serializationId;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    @Override
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    @Override
    public Enrollment getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(Enrollment enrollment) {
        this.enrollment = enrollment;
    }

    @Override
    public String getMspId() {
        return mspId;
    }

    public void setMspId(String mspId) {
        this.mspId = mspId;
    }

    public FabricOrganization getOrganization() {
        return organization;
    }

    public void setOrganization(FabricOrganization organization) {
        this.organization = organization;
    }

    public String getEnrollmentSecret() {
        return enrollmentSecret;
    }

    public void setEnrollmentSecret(String enrollmentSecret) {
        this.enrollmentSecret = enrollmentSecret;
    }

    @Override
    public String toString() {
        return "AppUser{" +
                "name='" + name + '\'' +
                ", roles=" + roles +
                ", account='" + account + '\'' +
                ", affiliation='" + affiliation + '\'' +
                ", organization='" + organization + '\'' +
                ", enrollmentSecret='" + enrollmentSecret + '\'' +
                ", enrollment=" + enrollment +
                ", mspId='" + mspId + '\'' +
                '}';
    }
}
