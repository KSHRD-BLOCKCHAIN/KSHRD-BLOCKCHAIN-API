package kh.com.kshrd.models.fabric;

import java.io.Serializable;

/**
 * Created by PENHCHET on 2018-03-25.
 */
public class FabricAdminOrganization implements Serializable {
    private String key;
    private String cert;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCert() {
        return cert;
    }

    public void setCert(String cert) {
        this.cert = cert;
    }
}
