package kh.com.kshrd.models.fabric;

import java.io.Serializable;

/**
 * Created by PENHCHET on 2018-03-25.
 */
public class FabricCA implements Serializable {
    private String hostname;
    private String url;
    private String mspId;

    public FabricCA() {

    }

    public FabricCA(String hostname, String url, String mspId) {
        this.hostname = hostname;
        this.url = url;
        this.mspId = mspId;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMspId() {
        return mspId;
    }

    public void setMspId(String mspId) {
        this.mspId = mspId;
    }

    @Override
    public String toString() {
        return "FabricCA{" +
                "hostname='" + hostname + '\'' +
                ", url='" + url + '\'' +
                ", mspId='" + mspId + '\'' +
                '}';
    }
}
