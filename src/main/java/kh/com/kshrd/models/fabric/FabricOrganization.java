package kh.com.kshrd.models.fabric;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PENHCHET on 2018-03-24.
 */
public class FabricOrganization implements Serializable {
    private String organizationName;
    private List<FabricPeer> fabricPeers = new ArrayList<>();
    private FabricCA fabricCA;
    private FabricAdminOrganization fabricAdminOrganization;

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizatoinName) {
        this.organizationName = organizatoinName;
    }

    public List<FabricPeer> getFabricPeers() {
        return fabricPeers;
    }

    public void setFabricPeers(List<FabricPeer> fabricPeers) {
        this.fabricPeers = fabricPeers;
    }

    public void addFabricPeer(FabricPeer fabricPeer) {
        this.fabricPeers.add(fabricPeer);
    }

    public FabricCA getFabricCA() {
        return fabricCA;
    }

    public void setFabricCA(FabricCA fabricCA) {
        this.fabricCA = fabricCA;
    }

    public FabricAdminOrganization getFabricAdminOrganization() {
        return fabricAdminOrganization;
    }

    public void setFabricAdminOrganization(FabricAdminOrganization fabricAdminOrganization) {
        this.fabricAdminOrganization = fabricAdminOrganization;
    }

    @Override
    public String toString() {
        return "FabricOrganization{" +
                "organizationName='" + organizationName + '\'' +
                ", fabricPeers=" + fabricPeers +
                ", fabricCA=" + fabricCA +
                ", fabricAdminOrganization=" + fabricAdminOrganization +
                '}';
    }
}
