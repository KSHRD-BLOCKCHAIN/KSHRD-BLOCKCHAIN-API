package kh.com.kshrd.models.fabric;

import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.TransactionException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by PENHCHET on 2018-03-20.
 */
public class HyperledgerFabricSDK {

    private static final Logger log = Logger.getLogger(HyperledgerFabricSDK.class);

    private static final String CHANNEL_NAME = "mychannel";
    private static final String ORDERER_ENDPOINT1 = "grpc://178.128.217.78:7050";
    private static final String ORDERER_NAME1 = "orderer1.kshrd.com.kh";
    private static final String ORDERER_ENDPOINT2 = "grpc://178.128.217.78:8050";
    private static final String ORDERER_NAME2 = "orderer2.kshrd.com.kh";
    private static final String ORDERER_ENDPOINT3 = "grpc://178.128.217.78:9050";
    private static final String ORDERER_NAME3 = "orderer3.kshrd.com.kh";

    /**
     * Initialize and get HF channel
     *
     * @param client The HFC client
     * @return Initialized channel
     * @throws InvalidArgumentException
     * @throws TransactionException
     */
    public static Channel getChannel(HFClient client, AppUser user) throws InvalidArgumentException, TransactionException {

        // initialize channel

        // orderer name and endpoint
        Orderer orderer1 = client.newOrderer(ORDERER_NAME1, ORDERER_ENDPOINT1);
        Orderer orderer2 = client.newOrderer(ORDERER_NAME2, ORDERER_ENDPOINT2);
        Orderer orderer3 = client.newOrderer(ORDERER_NAME3, ORDERER_ENDPOINT3);
        // channel name
        Channel channel = client.newChannel(CHANNEL_NAME);

        for (FabricPeer fabricPeer : user.getOrganization().getFabricPeers()) {
            // peer name and endpoint
            Peer peer = client.newPeer(fabricPeer.getHostname(), fabricPeer.getUrl());
            // eventhub name and endpoint
            EventHub eventHub = client.newEventHub(fabricPeer.getEventHubName(), fabricPeer.getEventHubUrl());
            channel.addPeer(peer);
            channel.addEventHub(eventHub);
        }

        channel.addOrderer(orderer1);
        channel.addOrderer(orderer2);
        channel.addOrderer(orderer3);
        channel.initialize();
        return channel;
    }

    /**
     * Create new HLF client
     *
     * @return new HLF client instance. Never null.
     * @throws CryptoException
     * @throws InvalidArgumentException
     */
    public static HFClient getHfClient() throws Exception {
        // initialize default cryptosuite
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        // setup the client
        HFClient client = HFClient.createNewInstance();
        client.setCryptoSuite(cryptoSuite);
        return client;
    }


    /**
     * Register and enroll user with userId.
     * If AppUser object with the name already exist on fs it will be loaded and
     * registration and enrollment will be skipped.
     *
     * @param caClient  The fabric-ca client.
     * @param registrar The registrar to be used.
     * @param userId    The user id.
     * @return AppUser instance with userId, affiliation,mspId and enrollment set.
     * @throws Exception
     */
    public static AppUser getUser(HFCAClient caClient, AppUser registrar, String userId) throws Exception {
        AppUser appUser = tryDeserialize(userId);
        if (appUser == null) {
            RegistrationRequest rr = new RegistrationRequest(userId, "coocon");
            rr.setAffiliation("org1.department1");
            rr.setSecret("secret");
            String enrollmentSecret = caClient.register(rr, registrar);
            Enrollment enrollment = caClient.enroll(userId, enrollmentSecret);
            appUser = new AppUser(userId, "coocon", "CooconMSP", enrollment);
            serialize(appUser);
        }
        return appUser;
    }

    public static AppUser getUser(HFCAClient caClient, String userId) throws Exception {
        AppUser appUser = tryDeserialize(userId);
        if (appUser == null) {
            System.out.println("No user you have to register first.");
        }
        return appUser;
    }

    /**
     * Enroll admin into fabric-ca using {@code admin/adminpw} credentials.
     * If AppUser object already exist serialized on fs it will be loaded and
     * new enrollment will not be executed.
     *
     * @param caClient The fabric-ca client
     * @return AppUser instance with userid, affiliation, mspId and enrollment set
     * @throws Exception
     */
    public static AppUser getAdmin(HFCAClient caClient) throws Exception {
        AppUser admin = tryDeserialize("admin");
        if (admin == null) {
            Enrollment adminEnrollment = caClient.enroll("admin", "admin");
            admin = new AppUser("admin", "coocon", "CooconMSP", adminEnrollment);
            serialize(admin);
        }
        return admin;
    }

    /**
     * Get new fabic-ca client
     *
     * @param caUrl              The fabric-ca-server endpoint url
     * @param caClientProperties The fabri-ca client properties. Can be null.
     * @return new client instance. never null.
     * @throws Exception
     */
    public static HFCAClient getHfCaClient(String caUrl, Properties caClientProperties) throws Exception {
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        HFCAClient caClient = HFCAClient.createNewInstance(caUrl, caClientProperties);
        caClient.setCryptoSuite(cryptoSuite);
        return caClient;
    }


    // user serialization and deserialization utility functions
    // files are stored in the base directory

    /**
     * Serialize AppUser object to file
     *
     * @param appUser The object to be serialized
     * @throws IOException
     */
    public static void serialize(AppUser appUser) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(
                Paths.get(appUser.getMspId() + appUser.getName() + ".jso")))) {
            oos.writeObject(appUser);
        }
    }

    /**
     * Deserialize AppUser object from file
     *
     * @param name The name of the user. Used to build file name ${name}.jso
     * @return
     * @throws Exception
     */
    public static AppUser tryDeserialize(String name) throws Exception {
        if (Files.exists(Paths.get(name + ".jso"))) {
            return deserialize(name);
        }
        return null;
    }

    public static AppUser deserialize(String name) throws Exception {
        try (ObjectInputStream decoder = new ObjectInputStream(
                Files.newInputStream(Paths.get(name + ".jso")))) {
            return (AppUser) decoder.readObject();
        }
    }

}
