package kh.com.kshrd.models.fabric;

import java.io.Serializable;

/**
 * Created by PENHCHET on 7/3/18.
 */
public class FabricPeer implements Serializable {
    private String hostname;
    private String url;
    private String eventHubName;
    private String eventHubUrl;

    public FabricPeer(String hostname, String url, String eventHubName, String eventHubUrl) {
        this.hostname = hostname;
        this.url = url;
        this.eventHubName = eventHubName;
        this.eventHubUrl = eventHubUrl;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEventHubName() {
        return eventHubName;
    }

    public void setEventHubName(String eventHubName) {
        this.eventHubName = eventHubName;
    }

    public String getEventHubUrl() {
        return eventHubUrl;
    }

    public void setEventHubUrl(String eventHubUrl) {
        this.eventHubUrl = eventHubUrl;
    }

    @Override
    public String toString() {
        return "FabricPeer{" +
                "hostname='" + hostname + '\'' +
                ", url='" + url + '\'' +
                ", eventHubName='" + eventHubName + '\'' +
                ", eventHubUrl='" + eventHubUrl + '\'' +
                '}';
    }
}
