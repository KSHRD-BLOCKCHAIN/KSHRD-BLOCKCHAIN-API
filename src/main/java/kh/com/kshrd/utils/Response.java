package kh.com.kshrd.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by DARAPENHCHET on 2/11/2017.
 */
public class Response implements Serializable {

    @JsonProperty("RESULT_CD")
    public String code;
    @JsonProperty("MESSAGE")
    public String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        switch (code) {
            case StatusCode.SUCCESS:
                message = "SUCCESSFULLY";
                break;
            case StatusCode.NOT_SUCCESS:
                message = "NOT SUCCESSFULLY";
                break;
            case StatusCode.NOT_FOUND:
                message = "NOT FOUND";
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Response [code=" + code + ", message=" + message + "]";
    }

}
